#ifndef CPU_H
#define CPU_H

#include <QStack>

#include "cartridge.h"
#include "display.h"

typedef struct {
    quint8 L;
    quint8 H;
} B8_HL;

union REGISTER_T {
    quint16 HL;
    B8_HL Bytes;
};

class CPU : QObject
{
    Q_OBJECT
public:
    CPU(Display *LCD);
    void loadCartridge(Cartridge *game, bool disasselbe = false);

    void run();
    bool runStep();

    //Register
    quint16 getAF() const;
    quint16 getBC() const;
    quint16 getDE() const;
    quint16 getHL() const;
    quint16 getSP() const;
    quint16 getPC() const;

    quint8 getOpCode() const;

    //Flags
    bool getZeroFlag() const;
    bool getAddSubFlag() const;
    bool getHalfCarryFlag() const;
    bool getCarryFlag() const;

    QString getLastStep() const;

    quint8 getIFF() const;

private:
    void init();
    bool exec();
    void CPn(quint8 n);

    //Components
    Cartridge *c;
    Display *d;
    QStack<quint16> stack;

    bool disassembling;
    QString lastStep;


    quint8 A;
    quint8 B;

    quint8 F;
    quint8 C;

    REGISTER_T HL;
    REGISTER_T DE;
    REGISTER_T BC;

    quint16 SP;
    quint16 PC;
    quint8 IFF;

    quint8 opCode;
    quint8 v8;
    quint16 v16;

    bool zeroFlag;
    bool addSubFlag;
    bool halfCarryFlag;
    bool carryFlag;


    void write8Bit(quint16 pos, quint8 val);
    void write16Bit(quint16 pos, quint16 val);
    quint8 read8Bit(quint16 pos);
    quint16 read16Bit(quint16 pos);
};

#endif // CPU_H
