#include "display.h"

Display::Display()
{
    Memory = QByteArray(0xFF55 - MEM_OFFSET_LCD, 0);

    write8Bit(0xFF40, 0x91)  ; //LCDC
    write8Bit(0xFF42, 0x00)   ; //SCY
    write8Bit(0xFF43, 0x00)   ; //SCX
    write8Bit(0xFF45, 0x00)   ; //LYC
    write8Bit(0xFF47, 0xFC)   ; //BGP
    write8Bit(0xFF48, 0xFF)   ; //OBP0
    write8Bit(0xFF49, 0xFF)   ; //OBP1
    write8Bit(0xFF4A, 0x00)   ; //WY
    write8Bit(0xFF4B, 0x00)   ; //WX
}



void Display::write16Bit(quint16 pos, quint16 val) {
    pos -= MEM_OFFSET_LCD;
    Memory[pos] =  val << 8;
    Memory[pos+1] = val;

    if(pos == 0 && (val&0x80)) {
        emit screenEnabled();
    }
}

void Display::write8Bit(quint16 pos, quint8 val) {
    pos -= MEM_OFFSET_LCD;
    Memory[pos] =  val;

    if(pos == 0 && (val&0x80)) {
        emit screenEnabled();
    }
}



quint16 Display::read16Bit(int i) {
    i -= MEM_OFFSET_LCD;
    return (Memory.at(i+1) << 8) + Memory.at(i);
}

quint8 Display::read8Bit(int i) {
    i -= MEM_OFFSET_LCD;
    return Memory.at(i);
}
