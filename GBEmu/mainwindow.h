#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "cartridge.h"
#include "cpu.h"

#include <QMainWindow>
#include <QFile>

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
public slots:
    void on_pb_LoadRom_clicked();
    void on_pb_disStep_clicked();    
    void on_pb_RunDA_clicked();

    void enableScreen();
private:
    Ui::MainWindow *ui;
    QByteArray romContent;
    QFile *romFile;

    CPU *cpu;
    Cartridge *currentCartridge;
    Display *currentDisplay;

    qint64 ranIns;

};

#endif // MAINWINDOW_H
