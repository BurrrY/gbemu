#ifndef DISPLAY_H
#define DISPLAY_H

#include <QByteArray>
#include <QDebug>

#define MEM_OFFSET_LCD 0xFF40

//Doc from: http://problemkaputt.de/pandocs.htm#videodisplay


class Display : public QObject

{
    Q_OBJECT
public:
    Display();

    void write8Bit(quint16 pos, quint8 val);
    void write16Bit(quint16 pos, quint16 val);
    quint16 read16Bit(int i);
    quint8 read8Bit(int i);
signals:
    screenEnabled();

private:
    QByteArray Memory;
};

#endif // DISPLAY_H
