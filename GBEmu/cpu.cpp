#include "cpu.h"

CPU::CPU(Display *LCD)
{
    d = LCD;
}

void CPU::loadCartridge(Cartridge *game, bool disasselbe)
{
    c = game;

    disassembling = disasselbe;
    init();

    if(disassembling)
        runStep();
    else
        run();
}

void CPU::init()
{
    PC = CR_ENRTYPOINT;
    A = 0x01;
    F = 0xB0;

    /*
    B = 0x00;
    C = 0x13;
    */

    BC.HL = 0x0013;
    HL.HL = 0x014D;
    DE.HL = 0x00D8;


    SP = 0xFFFE;

    zeroFlag = true;
    addSubFlag = true;
    halfCarryFlag = true;

    c->init();
}

void CPU::CPn(quint8 n)
{
    qint16 r = (A - n);
    addSubFlag = true;
    carryFlag = (r < 0);
    zeroFlag = (r == 0);
}

quint8 CPU::getIFF() const
{
    return IFF;
}

QString CPU::getLastStep() const
{
    return lastStep;
}

bool CPU::getCarryFlag() const
{
    return carryFlag;
}

bool CPU::getHalfCarryFlag() const
{
    return halfCarryFlag;
}

bool CPU::getAddSubFlag() const
{
    return addSubFlag;
}

bool CPU::getZeroFlag() const
{
    return zeroFlag;
}

quint8 CPU::getOpCode() const
{
    return opCode;
}

quint16 CPU::getPC() const
{
    return PC;
}

quint16 CPU::getSP() const
{
    return SP;
}

quint16 CPU::getHL() const
{
    return HL.HL;
}

quint16 CPU::getDE() const
{
    return DE.HL;
}

quint16 CPU::getBC() const
{
    return (B<<8) + C;
}

quint16 CPU::getAF() const
{
    return (A<<8) + F;
}


void CPU::write16Bit(quint16 pos, quint16 val) {
    if(pos >= 0xFF40 && pos <= 0xFF55)
        d->write16Bit(pos, val);
    else
        c->write16Bit(pos, val);
}

void CPU::write8Bit(quint16 pos, quint8 val) {
    if(pos >= 0xFF40 && pos <= 0xFF55)
        d->write8Bit(pos, val);
    else
        c->write8Bit(pos, val);
}



quint16 CPU::read16Bit(quint16 pos) {
    if(pos >= 0xFF40 && pos <= 0xFF55)
        return d->read16Bit(pos);
    else
        return c->read16Bit(pos);
}

quint8 CPU::read8Bit(quint16 pos) {
    if(pos >= 0xFF40 && pos <= 0xFF55)
        return d->read8Bit(pos);
    else
        return c->read8Bit(pos);
}


bool CPU::runStep() {


    opCode = read8Bit(PC);
    lastStep = "0x" + QString::number(PC, 16).toUpper();
    PC++;
    return exec();

}


void CPU::run() {

    for(;;) {
        opCode = read8Bit(PC);
        qDebug() << "oc: 0x" + QString::number(opCode, 16) << " @ " + QString::number(PC, 16);

        PC++;
        exec();
    }
}

bool CPU::exec() {
    switch (opCode) {
    case 0:
        lastStep +=  " NOP";
        break;
    case 0x01: //  LD bc, nn
        lastStep +=  " LD bc, " + QString::number(read16Bit(PC), 16).toUpper();;
        BC.HL = read16Bit(PC++);
        PC++;
        break;
    case 0x06: //  LD b, n
        lastStep +=  " LD b, 0x" + QString::number(read8Bit(PC), 16).toUpper();;
        B = read8Bit(PC++);
        break;
    case 0x0B: // DEC BC
        lastStep +=  "  DEC BC";
        BC.HL--;
        break;
    case 0x0C: // INC C
        lastStep +=  "  INC C";
        BC.Bytes.L++;
        zeroFlag = (BC.Bytes.L == 0);
        addSubFlag = false;
        break;
    case 0x16: //  LD d, n
        lastStep +=  " LD d, 0x" + QString::number(read8Bit(PC), 16).toUpper();;
        DE.Bytes.H = read8Bit(PC++);
        break;
    case 0x18: //  relative jump
        lastStep +=  " JR 0x" + QString::number(PC + read8Bit(PC) + 1, 16).toUpper();
        PC += read8Bit(PC)+1;
        break;
    case 0x1B: // DEC DE
        lastStep +=  "  DEC DE";
        DE.HL--;
        break;
    case 0x1C: // INC E
        lastStep +=  "  INC E";
        DE.Bytes.L++;
        zeroFlag = (DE.Bytes.L == 0);
        addSubFlag = false;
        break;
    case 0x20: //  conditional relative jump if nz
        lastStep +=  " JR NZ 0x" + QString::number(PC + read8Bit(PC) + 1, 16).toUpper();
        if(!zeroFlag)
            PC += read8Bit(PC)+1;
        else
            PC++;
        break;
    case 0x21: //  LD hl, nn
        lastStep +=  " LD hl, " + QString::number(read16Bit(PC), 16).toUpper();;
        HL.HL = read16Bit(PC++);
        PC++;
        break;
    case 0x23: // INC HL
        lastStep +=  " INC HL";
        HL.HL++;
        break;
    case 0x26: //  LD h, n
        lastStep +=  " LD h, 0x" + QString::number(read8Bit(PC), 16).toUpper();;
        HL.Bytes.H = read8Bit(PC++);
        break;
    case 0x28: //  conditional relative jump if z
        lastStep +=  " JR Z 0x" + QString::number(PC + read8Bit(PC) + 1, 16).toUpper();
        if(zeroFlag)
            PC += read8Bit(PC)+1;
        else
            PC++;
        break;
    case 0x2B: // DEC HL
        lastStep +=  "  DEC HL";
        HL.HL--;
        break;
    case 0x2C: // INC L
        lastStep +=  "  INC L";
        HL.Bytes.L++;
        zeroFlag = (HL.Bytes.L == 0);
        addSubFlag = false;
        break;
    case 0x2F: //    cpl              2F         4 -11- A = A xor FF
        lastStep +=  " A XOR 0xFF";
        A ^= 0xFF;
        halfCarryFlag = true;
        addSubFlag = true;
        break;
    case 0x31: //  LD sp, nn
        lastStep +=  " LD sp, " + QString::number(read16Bit(PC), 16).toUpper();;
        stack.push(read16Bit(PC++));
        PC++;
        break;
    case 0x36: //  ld   (HL),n      Write n to adress saved in HL
        lastStep +=  " ld (HL), " + QString::number(read8Bit(PC), 16).toUpper();
        write8Bit(HL.HL, read8Bit(PC++));
        break;
    case 0x38: //  conditional relative jump if C
        lastStep +=  " JR C 0x" + QString::number(PC + read8Bit(PC) + 1, 16).toUpper();
        if(carryFlag)
            PC += read8Bit(PC)+1;
        else
            PC++;
        break;
    case 0x37: //  Set Carry
        lastStep +=  " SCF";
        carryFlag = true;
        break;
    case 0x3B: // DEC SP
        lastStep +=  "  DEC SP";
        SP--;
        stack.pop();
        break;
    case 0x3C: // INC A
        lastStep +=  "  INC A";
        A++;
        zeroFlag = (A == 0);
        addSubFlag = false;
        break;
    case 0x3E: // LD    A,I         Load accumulator with I.
        lastStep +=  " LD A," + QString::number(read8Bit(PC), 16).toUpper();
        A = read8Bit(PC++);
        break;

        //LOAD INTO B
    case 0x40: // LD    B,B
        lastStep +=  " LD B,B";
        B = B;
        break;
    case 0x41: // LD    B,C
        lastStep +=  " LD B,C";
        B = C;
        break;
    case 0x42: // LD    B,D
        lastStep +=  " LD B,D";
        B = B;
        break;
    case 0x43: // LD    B,E
        lastStep +=  " LD B,E";
        B = C;
        break;
    case 0x44: // LD    B,H
        lastStep +=  " LD B,H";
        B = HL.Bytes.H;
        break;
    case 0x45: // LD    B,L
        lastStep +=  " LD B,L";
        B = HL.Bytes.L;
        break;
    case 0x46: // LD    B,(hl)"
        lastStep +=  " LD B,(hl)";
        B = read8Bit(HL.HL);
        break;
    case 0x47: // LD    B,A
        lastStep +=  " LD B,A";
        B = A;
        break;


        //LOAD INTO C
    case 0x48: // LD    C,B
        lastStep +=  " LD C,B";
        C = B;
        break;
    case 0x49: // LD    C,C
        lastStep +=  " LD E,C";
        C = C;
        break;
    case 0x4A: // LD    C,D
        lastStep +=  " LD E,D";
        C = B;
        break;
    case 0x4b: // LD    C,E
        lastStep +=  " LD E,E";
        C = C;
        break;
    case 0x4C: // LD    C,H
        lastStep +=  " LD E,H";
        C = HL.Bytes.H;
        break;
    case 0x4D: // LD    C,L
        lastStep +=  " LD E,L";
        C = HL.Bytes.L;
        break;
    case 0x4E: // LD    C,(hl)"
        lastStep +=  " LD C,(hl)";
        C = read8Bit(HL.HL);
        break;
    case 0x4F: // LD    C,A
        lastStep +=  " LD C,A";
        C = A;
        break;


        //LOAD INTO D
    case 0x50: // LD    D,B
        lastStep +=  " LD D,B";
        DE.Bytes.H = B;
        break;
    case 0x51: // LD    D,C
        lastStep +=  " LD D,C";
        DE.Bytes.H = C;
        break;
    case 0x52: // LD    D,D
        lastStep +=  " LD D,D";
        DE.Bytes.H = DE.Bytes.H;
        break;
    case 0x53: // LD    D,E
        lastStep +=  " LD D,E";
        DE.Bytes.H = DE.Bytes.L;
        break;
    case 0x54: // LD    D,H
        lastStep +=  " LD D,H";
        DE.Bytes.H = HL.Bytes.H;
        break;
    case 0x55: // LD    D,L
        lastStep +=  " LD D,L";
        DE.Bytes.H = HL.Bytes.L;
        break;
    case 0x56: // LD    D,(hl)"
        lastStep +=  " LD D,(hl)";
        DE.Bytes.H = read8Bit(HL.HL);
        break;
    case 0x57: // LD    D,A
        lastStep +=  " LD D,A";
        DE.Bytes.H = A;
        break;


        //LOAD INTO E
    case 0x58: // LD    E,B
        lastStep +=  " LD D,B";
        DE.Bytes.L = B;
        break;
    case 0x59: // LD    E,C
        lastStep +=  " LD E,C";
        DE.Bytes.L = C;
        break;
    case 0x5A: // LD    E,D
        lastStep +=  " LD E,D";
        DE.Bytes.L = DE.Bytes.H;
        break;
    case 0x5b: // LD    E,E
        lastStep +=  " LD E,E";
        DE.Bytes.L = DE.Bytes.L;
        break;
    case 0x5C: // LD    E,H
        lastStep +=  " LD E,H";
        DE.Bytes.L = HL.Bytes.H;
        break;
    case 0x5D: // LD    E,L
        lastStep +=  " LD E,L";
        DE.Bytes.L = HL.Bytes.L;
        break;
    case 0x5E: // LD    E,(hl)"
        lastStep +=  " LD E,(hl)";
        DE.Bytes.L = read8Bit(HL.HL);
        break;
    case 0x5F: // LD    E,A
        lastStep +=  " LD E,A";
        DE.Bytes.L = A;
        break;

        //LOAD INTO H
    case 0x60: // LD H,B
        lastStep +=  " LD H,B";
        HL.Bytes.H = B;
        break;
    case 0x61: // LD H,C
        lastStep +=  " LD H,C";
        HL.Bytes.H = C;
        break;
    case 0x62: // LD H,D
        lastStep +=  " LD H,D";
        HL.Bytes.H = HL.Bytes.H;
        break;
    case 0x63: // LD H,E
        lastStep +=  " LD H,E";
        HL.Bytes.H = HL.Bytes.L;
        break;
    case 0x64: // LD H,H
        lastStep +=  " LD H,H";
        HL.Bytes.H = HL.Bytes.H;
        break;
    case 0x65: // LD H,L
        lastStep +=  " LD H,L";
        HL.Bytes.H = HL.Bytes.L;
        break;
    case 0x66: // LD H,(hl)"
        lastStep +=  " LD H,(hl)";
        HL.Bytes.H = read8Bit(HL.HL);
        break;
    case 0x67: // LD H,A
        lastStep +=  " LD H,A";
        HL.Bytes.H = A;
        break;


        //LOAD INTO L
    case 0x68: // LD L,B
        lastStep +=  " LD L,B";
        HL.Bytes.L = B;
        break;
    case 0x69: // LD L,C
        lastStep +=  " LD L,C";
        HL.Bytes.L = C;
        break;
    case 0x6A: // LD L,D
        lastStep +=  " LD L,D";
        HL.Bytes.L = HL.Bytes.H;
        break;
    case 0x6b: // LD L,E
        lastStep +=  " LD L,E";
        HL.Bytes.L = HL.Bytes.L;
        break;
    case 0x6C: // LD L,H
        lastStep +=  " LD L,H";
        HL.Bytes.L = HL.Bytes.H;
        break;
    case 0x6D: // LD L,L
        lastStep +=  " LD L,L";
        HL.Bytes.L = HL.Bytes.L;
        break;
    case 0x6E: // LD L,(hl)"
        lastStep +=  " LD L,(hl)";
        HL.Bytes.L = read8Bit(HL.HL);
        break;
    case 0x6F: // LD L,A
        lastStep +=  " LD L,A";
        HL.Bytes.L = A;
        break;

        //LOAD INTO (hl)
    case 0x70: // LD (HL),B
        lastStep +=  " LD (HL),B";
        write8Bit(HL.HL,  B);
        break;
    case 0x71: // LD (HL),C
        lastStep +=  " LD (HL),C";
        write8Bit(HL.HL,  C);
        break;
    case 0x72: // LD (HL),D
        lastStep +=  " LD (HL),D";
        write8Bit(HL.HL,  HL.Bytes.H);
        break;
    case 0x73: // LD (HL),E
        lastStep +=  " LD (HL),E";
        write8Bit(HL.HL,  HL.Bytes.L);
        break;
    case 0x74: // LD (HL),H
        lastStep +=  " LD (HL),H";
        write8Bit(HL.HL,  HL.Bytes.H);
        break;
    case 0x75: // LD (HL),L
        lastStep +=  " LD (HL),L";
        write8Bit(HL.HL,  HL.Bytes.L);
        break;
    case 0x76: // HALT"
        lastStep +=  " HALT";
        exit(0);
        break;
    case 0x77: // LD (HL),A
        lastStep +=  " LD (HL),A";
        write8Bit(HL.HL,  A);
        break;


        //LOAD INTO A
    case 0x78: // LD A,B
        lastStep +=  " LD A,B";
        A = B;
        break;
    case 0x79: // LD A,C
        lastStep +=  " LD A,C";
        A = C;
        break;
    case 0x7A: // LD A,D
        lastStep +=  " LD A,D";
        A = HL.Bytes.H;
        break;
    case 0x7b: // LD A,E
        lastStep +=  " LD A,E";
        A = HL.Bytes.L;
        break;
    case 0x7C: // LD A,H
        lastStep +=  " LD A,H";
        A = HL.Bytes.H;
        break;
    case 0x7D: // LD A,L
        lastStep +=  " LD A,L";
        A = HL.Bytes.L;
        break;
    case 0x7E: // LD A,(hl)"
        lastStep +=  " LD A,(hl)";
        A = read8Bit(HL.HL);
        break;
    case 0x7F: // LD A,A
        lastStep +=  " LD A,A";
        A = A;
        break;



    case 0xAF: // XOR A
        lastStep +=  " XOR A";
        A ^= A;
        zeroFlag = true;
        carryFlag = false;
        break;
    case 0xB0: // OR B
        lastStep +=  " OR B";
        A |= B;
        zeroFlag = (A == 0);
        carryFlag = false;
        addSubFlag = false;
        halfCarryFlag = false;
        break;
    case 0xB1: // OR C
        lastStep +=  " OR C";
        A |= C;
        zeroFlag = (A == 0);
        carryFlag = false;
        addSubFlag = false;
        halfCarryFlag = false;
        break;
    case 0xB8: //  cp   e           FE nn      8 z1hc compare A-n
        CPn(DE.Bytes.L);
        lastStep +=  " CP E";
        break;
    case 0xC3: //jp   nn        C3 nn nn    16 ---- jump to nn, PC=nn
        PC = read16Bit(PC);
        lastStep +=  " JP 0x" + QString::number(PC, 16).toUpper();
        break;
    case 0xC6: //ADD A,N
    {
        lastStep +=  " ADD A, 0x" + QString::number(read8Bit(PC), 16).toUpper();
        qint8 aOld = A;
        qint8 N = read8Bit(PC++);
        A += N;
        zeroFlag = (A == 0);
        carryFlag = ( (A-N)!=aOld );
        addSubFlag = false;

        //TODO: set if carry from 3rd bit
        halfCarryFlag = false;
    }
        break;
    case 0xC9: //RET
        PC = stack.pop();
        lastStep +=  " RET (" + QString::number(PC, 16).toUpper() + ")";
        break;
    case 0xCB: // One of Many
        //http://www.myquest.nl/z80undocumented/z80cpu_um.pdf Page 254
        {
            quint8 nB = read8Bit(PC++);

            switch (nB) {
            case 0x87: //Clear First bit in A
                lastStep +=  " RES 0, A";
                A &= ~(0x01);
                break;
            case 0x37: //SWAP A
            {
                lastStep +=  " SWAP A";
                quint8 tmp = (A & 0xF0) >> 4;
                A = (A << 4) + tmp;
                zeroFlag = (A == 0);
                carryFlag = false;
                halfCarryFlag = false;
                addSubFlag = false;
            }
                break;
            default:
                lastStep +=  " UNKNOWN: " + QString::number(opCode, 16).toUpper() + QString::number(nB, 16).toUpper();
                qCritical() << "Unhandeld Instruction: " << QString::number(opCode, 16).toUpper() + QString::number(nB, 16).toUpper();
                break;
            }
        }
        break;
    case 0xCD: //CALL  nz,nn       Call subroutine at location nn if condition CC is true.
        lastStep +=  " CALL NZ 0x" + QString::number(read16Bit(PC), 16).toUpper();
        if(!zeroFlag) {
            stack.push(PC+1);
            PC = read16Bit(PC);
        }
        else
            PC += 2;
        break;
    case 0xE0: //LD   (FF00+n), A  E0 nn     12 ---- write to io-port n (memory FF00+n)
        //TODO: Verify Write
        lastStep +=  " LD (0x" + QString::number(0xFF00 + read8Bit(PC), 16).toUpper() + "), A";
        write8Bit(0xFF00 + read8Bit(PC++), A);
        break;
    case 0xE6: //  and  n           E6 nn      8 z010 A=A & n
        lastStep +=  " AND 0x" + QString::number(read8Bit(PC), 16).toUpper();
        A &= (read8Bit(PC++));
        zeroFlag = (A == 0);
        carryFlag = false;
        halfCarryFlag = true;
        addSubFlag = false;
        break;
    case 0xEA: // LD    (nn), A      Load location (nn) with accumulator.
        //TODO: Verify Write
        lastStep +=  " LD (0x" + QString::number(read16Bit(PC), 16).toUpper() + "), A";
        write8Bit(read16Bit(PC), A);
        PC+=2;
        break;
    case 0xF0: // LD   A,(FF00+n)  F0 nn     12 ---- read from io-port n (memory FF00+n)
        lastStep +=  " LD A, (0x" + QString::number(0xFF00 + read8Bit(PC), 16).toUpper() + ")";
        A = read8Bit(0xFF00 + read8Bit(PC++));
        break;
    case 0xF3: //  DI                Disable interrupts.
        IFF = 0x00;
        lastStep +=  " DI";
        break;
    case 0xFE: //  cp   n           FE nn      8 z1hc compare A-n
        CPn(read8Bit(PC));
        lastStep +=  " CP 0x" + QString::number(read8Bit(PC++), 16).toUpper();
        break;
    case 0xFF: //  RST 0x38
        lastStep +=  " RST 0x38";
        stack.push(PC);
        PC = 0x38;
        break;
    default:
        lastStep +=  " UNKNOWN: " + QString::number(opCode, 16).toUpper();
        qCritical() << "Unhandeld Instruction: " << QString::number(opCode, 16).toUpper();
        return false;
        break;
    }

    return true;

}
