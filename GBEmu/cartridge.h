#ifndef CARTRIDGE_H
#define CARTRIDGE_H

#include <QByteArray>
#include <QString>
#include <QDebug>
#include <QBitmap>
#include <QPixmap>
#include <QChar>


#define CR_TITLE_START 0x134
#define CR_TITLE_LENGTH 0x143-CR_TITLE_START

#define CR_NINTENDOLOGO_START 0x104
#define CR_NINTENDOLOGO_LENGTH 0x133-CR_NINTENDOLOGO_START

#define CR_TYPE_START 0x147
#define CR_ROMSIZE_START 0x148
#define CR_RAMSIZE_START 0x149

#define CR_ENRTYPOINT 0x100


class Cartridge
{
public:
    Cartridge(QByteArray d);


    qint64 size();
    char at(int i);

    QString getGameTitle() const;
    QBitmap getNintendoLogo() const;

    int getType() const;

    int getRomSize() const;

    int getRamSize() const;

    quint16 read16Bit(int i);
    quint8 read8Bit(int i);
    void write8Bit(quint16 pos, quint8 val);
    void write16Bit(quint16 pos, quint16 val);
    void init();
private:
    void setNintendoLogo(const QByteArray &value);
    void setGameTitle(const QByteArray &value);
    void setType(const int &value);
    void setRomSize(int value);
    void setRamSize(int value);

    QByteArray CartridgeData;

    QBitmap nintendoLogo;//0104-0133
    QString gameTitle; //0134-0143
    int Type;
    int RomSize;
    int RamSize;



};

#endif // CARTRIDGE_H
