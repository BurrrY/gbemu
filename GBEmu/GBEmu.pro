#-------------------------------------------------
#
# Project created by QtCreator 2016-06-20T20:51:26
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = GBEmu
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    cartridge.cpp \
    cpu.cpp \
    display.cpp

HEADERS  += mainwindow.h \
    cartridge.h \
    cpu.h \
    display.h

FORMS    += mainwindow.ui
