#include "cartridge.h"

Cartridge::Cartridge(QByteArray d)
{
    CartridgeData = d;
    setGameTitle(CartridgeData.mid(CR_TITLE_START, CR_TITLE_LENGTH));
    setNintendoLogo(CartridgeData.mid(CR_NINTENDOLOGO_START, CR_NINTENDOLOGO_LENGTH));
    setType(CartridgeData.at(CR_TYPE_START));
    setRomSize(CartridgeData.at(CR_ROMSIZE_START));
    setRamSize(CartridgeData.at(CR_RAMSIZE_START));
}


void Cartridge::init() {
    CartridgeData[0xFF05] = 0x00   ;// TIMA
    CartridgeData[0xFF06] = 0x00   ; //TMA
    CartridgeData[0xFF07] = 0x00   ; //TAC
    CartridgeData[0xFF10] = 0x80   ; //NR10
    CartridgeData[0xFF11] = 0xBF   ; //NR11
    CartridgeData[0xFF12] = 0xF3   ; //NR12
    CartridgeData[0xFF14] = 0xBF   ; //NR14
    CartridgeData[0xFF16] = 0x3F   ; //NR21
    CartridgeData[0xFF17] = 0x00   ; //NR22
    CartridgeData[0xFF19] = 0xBF   ; //NR24
    CartridgeData[0xFF1A] = 0x7F   ; //NR30
    CartridgeData[0xFF1B] = 0xFF   ; //NR31
    CartridgeData[0xFF1C] = 0x9F   ; //NR32
    CartridgeData[0xFF1E] = 0xBF   ; //NR33
    CartridgeData[0xFF20] = 0xFF   ; //NR41
    CartridgeData[0xFF21] = 0x00   ; //NR42
    CartridgeData[0xFF22] = 0x00   ; //NR43
    CartridgeData[0xFF23] = 0xBF   ; //NR30
    CartridgeData[0xFF24] = 0x77   ; //NR50
    CartridgeData[0xFF25] = 0xF3   ; //NR51
    CartridgeData[0xFF26] = 0xF1   ; //$F1-GB, $F0-SGB ; NR52

    CartridgeData[0xFFFF] = 0x00   ; //IE
}

void Cartridge::setGameTitle(const QByteArray &value)
{
    gameTitle = value;
}

int Cartridge::getType() const
{
    return Type;
}

void Cartridge::setType(const int &value)
{
    Type = value;
}

int Cartridge::getRamSize() const
{
    return RamSize;
}

void Cartridge::setRamSize(int value)
{
    RamSize = value;
}

int Cartridge::getRomSize() const
{
    return RomSize;
}

void Cartridge::setRomSize(int value)
{
    RomSize = value;
}

QString Cartridge::getGameTitle() const
{
    return gameTitle;
}

QBitmap Cartridge::getNintendoLogo() const
{
    return nintendoLogo;
}

void Cartridge::setNintendoLogo(const QByteArray &value)
{
    nintendoLogo.loadFromData(value);
}


qint64 Cartridge::size() {
    return CartridgeData.size();
}

char Cartridge::at(int i)
{
    return CartridgeData.at(i);
}


quint16 Cartridge::read16Bit(int i) {
    return (CartridgeData.at(i+1) << 8) + CartridgeData.at(i);
}

quint8 Cartridge::read8Bit(int i) {
    return CartridgeData.at(i);
}

void Cartridge::write16Bit(quint16 pos, quint16 val) {
    CartridgeData[pos] =  val << 8;
    CartridgeData[pos+1] = val;
}

void Cartridge::write8Bit(quint16 pos, quint8 val) {
    CartridgeData[pos] =  val;
}
