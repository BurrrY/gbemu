#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QFileDialog>
#include <QDebug>
#include <QGraphicsScene>
#include <QThread>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    currentDisplay = new Display();
    cpu = new CPU(currentDisplay);

    connect(currentDisplay, &Display::screenEnabled, this, &MainWindow::enableScreen);

    ranIns = 0;
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pb_LoadRom_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open ROM"));
    romFile = new QFile(fileName);
    if(romFile->exists()) {
        romFile->open(QIODevice::ReadOnly);
        currentCartridge = new Cartridge(romFile->readAll());
        romFile->close();


        int romSize = currentCartridge->size();
        ui->lbl_romsize->setText(QString::number(romSize) + "bytes");
        ui->lbl_gameTitle->setText(currentCartridge->getGameTitle());
        ui->lbl_romSize->setText(QString::number(currentCartridge->getRomSize()) + "b");


        switch (currentCartridge->getRamSize()) {
        case 0x00:
            ui->lbl_ramSize->setText("None");
            break;
        case 0x01:
            ui->lbl_ramSize->setText("2 KBytes");
            break;
        case 0x02:
            ui->lbl_ramSize->setText("8 KBytes");
            break;
        case 0x03:
            ui->lbl_ramSize->setText("32 KBytes");
            break;
        default:
            break;
        }

        switch (currentCartridge->getType()) {
        case 0x00:
            ui->lbl_cartridgeType->setText("ROM ONLY");
            break;
        case 0x01:
            ui->lbl_cartridgeType->setText("MBC1");
            break;
        case 0x02:
            ui->lbl_cartridgeType->setText("MBC1+RAM");
            break;
        case 0x03:
            ui->lbl_cartridgeType->setText("MBC1+RAM+BATTERY");
            break;
        case 0x05:
            ui->lbl_cartridgeType->setText("MBC2");
            break;
        case 0x06:
            ui->lbl_cartridgeType->setText("MBC2+BATTERY");
            break;
        case 0x08:
            ui->lbl_cartridgeType->setText("ROM+RAM");
            break;
        case 0x09:
            ui->lbl_cartridgeType->setText("ROM+RAM+BATTERY");
            break;
        case 0x0B:
            ui->lbl_cartridgeType->setText("MMM01");
            break;
        case 0x0C:
            ui->lbl_cartridgeType->setText("MMM01+RAM");
            break;
        case 0x0D:
            ui->lbl_cartridgeType->setText("MMM01+RAM+BATTERY");
            break;
        case 0x13:
            ui->lbl_cartridgeType->setText("MBC3+RAM+BATTERY");
            break;
        default:
            ui->lbl_cartridgeType->setText("0x" + QString::number(currentCartridge->getType(), 16).right(2));
            break;
        }

    /*   QGraphicsScene *scene = new QGraphicsScene(this);
       scene->addPixmap(currentCartridge->getNintendoLogo());
       scene->setSceneRect(currentCartridge->getNintendoLogo().rect());
       ui->gv_logo->setScene(scene);*/

/*
        int colCnt = 16;
        for(int row=0;row < romSize/colCnt;row++) {
            QString data = "0x" + QString::number(row * colCnt, 16) + "\t";
            for(int col=0;col < colCnt; col++) {
              data += QString::number(currentCartridge->at(col + (row * colCnt)), 16).right(2) + "\t";
            }
            data += "\n";
            ui->pte_RomContent->appendPlainText(data);
        }
*/

        bool dis = (bool)ui->cb_Disassemble->checkState();
        cpu->loadCartridge(currentCartridge, dis);
        if(dis) {
            ui->lbl_AF->setText("0x" + QString::number(cpu->getAF(), 16).toUpper());
            ui->lbl_BC->setText("0x" + QString::number(cpu->getBC(), 16).toUpper());
            ui->lbl_DE->setText("0x" + QString::number(cpu->getDE(), 16).toUpper());
            ui->lbl_HL->setText("0x" + QString::number(cpu->getHL(), 16).toUpper());

            ui->lbl_PC->setText("0x" + QString::number(cpu->getPC(), 16).toUpper());
            ui->lbl_SP->setText("0x" + QString::number(cpu->getSP(), 16).toUpper());

            ui->lbl_IFF->setText("0x" + QString::number(cpu->getIFF(), 2).right(4));


            ui->cb_Carry->setChecked(cpu->getCarryFlag());
            ui->cb_ZeroFlag->setChecked(cpu->getZeroFlag());
            ui->cb_flagH->setChecked(cpu->getHalfCarryFlag());
            ui->cb_flagN->setChecked(cpu->getAddSubFlag());

            ui->ptw_disassemble->appendPlainText(cpu->getLastStep());
        }
    }
}

void MainWindow::on_pb_disStep_clicked()
{

    cpu->runStep();

    ui->lbl_AF->setText("0x" + QString::number(cpu->getAF(), 16).toUpper());
    ui->lbl_BC->setText("0x" + QString::number(cpu->getBC(), 16).toUpper());
    ui->lbl_DE->setText("0x" + QString::number(cpu->getDE(), 16).toUpper());
    ui->lbl_HL->setText("0x" + QString::number(cpu->getHL(), 16).toUpper());

    ui->lbl_PC->setText("0x" + QString::number(cpu->getPC(), 16).toUpper());
    ui->lbl_SP->setText("0x" + QString::number(cpu->getSP(), 16).toUpper());

    ui->lbl_IFF->setText("0x" + QString::number(cpu->getIFF(), 2).right(4));

    ui->cb_Carry->setChecked(cpu->getCarryFlag());
    ui->cb_ZeroFlag->setChecked(cpu->getZeroFlag());
    ui->cb_flagH->setChecked(cpu->getHalfCarryFlag());
    ui->cb_flagN->setChecked(cpu->getAddSubFlag());
    ui->lbl_insCnt->setText(QString::number(ranIns++));

    ui->ptw_disassemble->appendPlainText(cpu->getLastStep());
}

void MainWindow::on_pb_RunDA_clicked()
{
    bool insOK = true;
    while(insOK) {
        insOK = cpu->runStep();

        ui->lbl_AF->setText("0x" + QString::number(cpu->getAF(), 16).toUpper());
        ui->lbl_BC->setText("0x" + QString::number(cpu->getBC(), 16).toUpper());
        ui->lbl_DE->setText("0x" + QString::number(cpu->getDE(), 16).toUpper());
        ui->lbl_HL->setText("0x" + QString::number(cpu->getHL(), 16).toUpper());

        ui->lbl_PC->setText("0x" + QString::number(cpu->getPC(), 16).toUpper());
        ui->lbl_SP->setText("0x" + QString::number(cpu->getSP(), 16).toUpper());

        ui->lbl_IFF->setText("0x" + QString::number(cpu->getIFF(), 2).right(4));

        ui->cb_Carry->setChecked(cpu->getCarryFlag());
        ui->cb_ZeroFlag->setChecked(cpu->getZeroFlag());
        ui->cb_flagH->setChecked(cpu->getHalfCarryFlag());
        ui->cb_flagN->setChecked(cpu->getAddSubFlag());

        ui->ptw_disassemble->appendPlainText(cpu->getLastStep());

        ui->lbl_insCnt->setText(QString::number(ranIns++));

        for(int i=0; i < 3; i++)  {
         //   QThread::usleep ( 100 );
            ui->ptw_disassemble->update();
        }
    }
}

void MainWindow::enableScreen()
{
    ui->gv_Screen->setEnabled(true);
}
